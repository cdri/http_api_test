# HTTP API Testing Container
This is a Ubuntu Linux with language-agnostic command-line tool for validating API description document against backend implementation of the API.

* Git - https://git-scm.com
* Node.js/npm - https://nodejs.org/en/
* Dredd - http://dredd.readthedocs.io/en/latest/
* Drakov - https://github.com/Aconex/drakov
* HTTPie - https://httpie.org
* jq - https://stedolan.github.io/jq/
* pip - https://pypi.org/project/pip/
