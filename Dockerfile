FROM ubuntu:18.04
MAINTAINER "Thiago Pereira Rosa <thiagor [at] engineer.com>"

RUN apt-get update \
    && apt-get install -y curl git python-pip jq lsb-release
RUN curl -sL https://deb.nodesource.com/setup_12.x | bash
RUN apt-get install -y nodejs
RUN pip install --upgrade pip setuptools httpie
RUN npm install -g dredd drakov

CMD /bin/bash
